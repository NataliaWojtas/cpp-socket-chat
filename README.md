How to run?  
1. download  
2. set-up in qt  
3. run 2 apps  
4. In first one:  
- click "Search"  
- write any port you want (ex. 1234)  
- click "Start Server"  
5. In second one:  
- write IP and port from first one  
- click "Search"  
- click "Connect"  
6. Enjoy chatting! ;)