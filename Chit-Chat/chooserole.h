#ifndef CHOOSEROLE_H
#define CHOOSEROLE_H

#include <QDialog>

namespace Ui {
class ChooseRole;
}

class ChooseRole : public QDialog
{
    Q_OBJECT

public:
    explicit ChooseRole(QWidget *parent = 0);
    ~ChooseRole();
    bool isServer;

private slots:
    
    void on_client_1_button_clicked();
    void on_client_2_button_clicked();

private:
    Ui::ChooseRole *ui;


};

#endif // CHOOSEROLE_H
