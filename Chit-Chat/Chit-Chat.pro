#-------------------------------------------------
#
# Project created by QtCreator 2017-01-27T11:21:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Chit-Chat
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    chooserole.cpp

HEADERS  += mainwindow.h \
    chooserole.h

FORMS    += mainwindow.ui \
    chooserole.ui
