#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>
#include <QString>
#include <QFile>
#include "chooserole.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionExit_triggered();
    void on_actionClear_triggered();
    void on_actionSave_triggered();
    void on_btn_connect_clicked();

    void connected();
    void disconnected();
    void newConnection();
    void readData();
    void getFile();
    void showHistory();

    void on_btn_send_clicked();
    void on_btn_start_clicked();
    void on_btn_send_file_clicked();

private:
    Ui::MainWindow *ui;

    QTcpServer *server;
    QTcpSocket *socket;
    ChooseRole *choose;

    bool condition;
    QString prt;
    QString adr;

    void findIP();
    void start();
    void stop();
    void initServer();
    void initSocket();

};

#endif // MAINWINDOW_H
