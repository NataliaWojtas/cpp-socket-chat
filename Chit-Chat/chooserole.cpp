#include "chooserole.h"
#include "ui_chooserole.h"

ChooseRole::ChooseRole(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChooseRole)
{
    ui->setupUi(this);
}

ChooseRole::~ChooseRole()
{
    delete ui;
}


void ChooseRole::on_client_1_button_clicked()
{
    isServer = true;
    this->close();
}

void ChooseRole::on_client_2_button_clicked()
{
    isServer = false;
    this->close();
}
