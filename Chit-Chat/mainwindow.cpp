#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtNetwork/QHostAddress>
#include <QNetworkInterface>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QTime>
#include <QDir>
#include "chooserole.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    socket = new QTcpSocket(this);
    server = new QTcpServer(this);

    choose = new ChooseRole();
    choose->exec();

    qDebug() << "jestem w mainie i szukam ip";

    QString filename = QDir::currentPath() + "/../lets_talk_about_it/Chit-Chat/tmp/server_list.txt";
    qDebug() << filename;
    QFile f(filename);
    f.open(QIODevice::ReadOnly);
    QTextStream in(&f);


    while (!in.atEnd())
    {
        QString line = in.readLine();
        qDebug() << line;


        prt = line.section(';',-1);
        adr = line.section(';',-2,-2);
    }
    f.close();

    if(f.size() == 0)
    {
        findIP();
    }
    else
    {
        if(choose->isServer)
        {
            ui->led_ip->setText(adr);
            ui->led_port->setText(prt);
            on_btn_start_clicked();
        }
        else
        {
            ui->led_ip->setText(adr);
            ui->led_port->setText(prt);
            on_btn_connect_clicked();
        }
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initSocket() {

    qDebug() << "jestem w init socket";
    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));

}


void MainWindow::newConnection() {

    qDebug() << "jestem w newconnection";
    socket = server->nextPendingConnection();
    ui->label_reciveText->setText("Connected :)\n-----------\n\n");
    showHistory();

    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));

//    connect(socket, SIGNAL(), this, SLOT())
}

void MainWindow::connected() {

    qDebug() << "jestem w connecet";
    ui->btn_send->setEnabled(true);
    ui->btn_send_file->setEnabled(true);
    ui->btn_connect->setText("Disconnect");
    ui->label_reciveText->setText("Connected :)\n-----------\n\n");
    showHistory();
    condition = true;

    QString adr = ui->led_ip->text();
    QString ptr = ui->led_port->text();

    QString filename = QDir::currentPath() + "/../lets_talk_about_it/Chit-Chat/tmp/server_list.txt";
    QFile f(filename);
    f.open( QIODevice::WriteOnly | QIODevice::Truncate);

    QTextStream stream(&f);

    QString  text = adr + ";" + ptr;
    stream << text;
    f.close();


}

void MainWindow::disconnected() {

    qDebug() << "jestem w disconnecet";
    ui->btn_send->setEnabled(false);
    ui->btn_send_file->setEnabled(false);
    ui->btn_connect->setText("Connect");
    condition = false;

}

void MainWindow::readData()
{
    qint64 size = 1;
    char read_type;
    socket->read(&read_type, size);

    int int_read_type = static_cast<int>(read_type);
    qDebug() << "jestem w readData(), int read type: " << int_read_type;

    if(int_read_type == 0)
    {
        QString data(socket->readAll());
        ui->label_reciveText->setText(ui->label_reciveText->text() + data);
    }
    else
    {
        getFile();
    }
}

void MainWindow::on_actionClear_triggered() {

    ui->label_reciveText->clear();

}

void MainWindow::on_actionExit_triggered() {

    MainWindow::close();

}

void MainWindow::on_actionSave_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save"), "/home/student", tr("Text Files (*.txt)"));
    QFile f(filename);
    f.open( QIODevice::WriteOnly | QIODevice::Append);

    QTextStream stream(&f);

    QString  text = "Date: " + QDateTime::currentDateTime().toString("dd/MM/yy hh:mm:ss");
    stream << text << endl;
    text = ui->label_reciveText->text() + ui->led_sendText->text().toUtf8();
    stream << text << endl;

    f.close();


}

void MainWindow::on_btn_connect_clicked()
{
    qDebug() << "jestem w connect clicked";
    initSocket();
    ui->btn_start->setEnabled(false);
    QHostAddress address(ui->led_ip->text());

    if (!condition) {

        socket->connectToHost(address, ui->led_port->text().toInt());

    }

    else {

        socket->disconnectFromHost();

    }
}


void MainWindow::findIP()
{
    qDebug() << "jestem w findIP";
    bool first = true;

    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {

        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)) {

            if(first) {

                ui->label_reciveText->setText(+ "These Servers are found:\n"+ address.toString());

                ui->led_ip->setText(address.toString());
                first = false;

            }

            else {
                ui->label_reciveText->setText(ui->label_reciveText->text()+ "\n" + address.toString());
            }
        }
    }

    if (first) {

        ui->label_reciveText->setText("We Cant find any Server. You can use Localhost IP.\n"
                                      "Localhost IP is: 127.0.0.1\n-------------------------------------\n");

        ui->led_ip->setText("127.0.0.1");

    }

    else {

        ui->label_reciveText->setText(ui->label_reciveText->text()
                                      + "\n--------------------\n");
    }
}

void MainWindow::on_btn_send_clicked()
{
  //  ui->led_sendText->setFocusPolicy(Qt::StrongFocus);
    qint64 type_size = 1;

    int type_file = 0;
    socket->write((const char*)&type_file, type_size);

    QString old_text = ui->label_reciveText->text() + "[Me]: " + ui->led_sendText->text().toUtf8();
    QString data(socket->readAll());

    socket->write("[You]: " + ui->led_sendText->text().toUtf8() + "\n");
    ui->label_reciveText->setText(old_text + "\n" + data);

    //saving conversation to file
    QString filename = QDir::currentPath() + "/../lets_talk_about_it/Chit-Chat/tmp/" + adr + "-" + prt;//nazwa pliku
    QFile f(filename);
    f.open( QIODevice::WriteOnly);

    QTextStream stream(&f);
    stream << old_text << endl;

    f.close();


    ui->led_sendText->clear();
}


void MainWindow::on_btn_start_clicked()
{
    qDebug() << "jestem w start clicked";
    ui->btn_connect->setEnabled(false);
    if (!condition)
        start();
    else
        stop();
}

void MainWindow::start() {

    qDebug() << "jestem w start";
    initServer();

    ui->btn_start->setText("Stop");
    ui->btn_send->setEnabled(true);
    ui->btn_send_file->setEnabled(true);

    condition = true;

}

void MainWindow::stop() {

    qDebug() << "jestem w stop";
    server->close();
    socket->close();

    ui->btn_start->setText("Start");
    ui->btn_send->setEnabled(false);
    ui->btn_send_file->setEnabled(false);


    condition = false;

}

void MainWindow::initServer() {
    qDebug() << "jestem w init server";

    server->listen(QHostAddress::Any, ui->led_port->text().toInt());

    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));

}

void MainWindow::on_btn_send_file_clicked()
{

    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_4_0);

    qint64 type_size = 1;

    //Sending a file
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"));
    QFile *file= new QFile(fileName);
    file->open(QIODevice::ReadOnly);
    qint64 filesize = file->size();

    //We'll send the file in chunks of 64k (short of). We don't want to fill the memory at once if it's a large file
    qint64 chunk = 64000;
    qint64 filepos=0;                                 //Position for reading/writing

    QString onlyFileName = fileName.section("/", -1, -1);

    int fileSize = onlyFileName.size();

    qint64 size = onlyFileName.size();

    //write length of name of the file
    socket->write((const char*)&fileSize, type_size);

    //write name of the file
    QString fsize = (const char*)&size;
    onlyFileName.prepend(fsize);
    socket->write(onlyFileName.toUtf8(), size + 1);

    qDebug() << "SEND FILE wysylana nazwa: " << onlyFileName.toUtf8() << " dlugosc: " << size;


    while (filepos < filesize)
    {
        char *data = new char[chunk];
        qint64 length = file->read(data, chunk);
        socket->write(data,length);                   //Send read data
        filepos += length;                            //Go to new position depending what we read
        delete [] data;
    }

    file->close();
    delete file;                                      //Free memory
}

void MainWindow::getFile()                            //Receiving data
{
    QDataStream in(socket);
    qint64 chunk = 64000;
    in.setVersion(QDataStream::Qt_4_0);

    qint64 type = 1;
    char cFileSize;

    socket->read(&cFileSize, type);                     //read file size

    qint64 fileSize = static_cast<qint64>(cFileSize);

    char *fileName = new char[fileSize];

    socket->read(fileName, fileSize);                   //read file name

    fileName[fileSize] = '\0';
    QString QfileName = fileName;

    qDebug() << "GET FILE fileSize: " << fileSize << "cFileSize: " << cFileSize << " fileName: " << QfileName;

    QString path = QFileDialog::getSaveFileName(this, tr("Save file"), "home/student/" + QfileName);
    QFile *file = new QFile(path);                     //Create the file
    file->open(QIODevice::WriteOnly);                  //Open it
    int length;

    //Download it in chunks, so the memory won't crash
    do
    {
        char *data = new char[chunk];
        length = socket->read(data, chunk);
        file->write(data,length);
        delete [] data;
    }while(length == chunk);

    file->close();
    delete file;
    delete fileName;
}

void MainWindow::showHistory()
{
    QDir myDir(QDir::currentPath() + "/../lets_talk_about_it/Chit-Chat/tmp/");
    QStringList filesList = myDir.entryList();
    QString name = QDir::currentPath() + "/../lets_talk_about_it/Chit-Chat/tmp/" + adr + "-" + prt;
    if( filesList.indexOf(adr + "-" + prt) != -1 )
    {
        QFile f2(name);
        f2.open(QIODevice::ReadOnly | QIODevice::Text );
        QTextStream in(&f2);

        QString data = in.readAll();
        ui->label_reciveText->setText(data);
        f2.close();
    }

}
